-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Control.List.Trans.EitherT(
  EitherT,
  List(nil, cons, uncons),
  ToHaskellList(toHaskellList),
  lift,
  liftEither,
  hideEither,
  run,
  down,
  downEither,
  throw,
  catch)
where

import Control.List(List(nil,cons,uncons), ToHaskellList(toHaskellList))
import qualified Control.List as List(map)
import qualified Control.Monad as Monad(join)

data EitherT b m a = EitherT (m (Either b a))

wrap = EitherT
unwrap (EitherT x) = x

instance List m => List (EitherT b m) where
  nil = wrap $ nil
  cons x xs = wrap $ cons (Right x) (unwrap xs)
  uncons  nil' cons' xs = wrap $
    uncons
      (unwrap nil')
      (\e xs' -> case e of
        Left x -> cons (Left x) (unwrap $ uncons nil' cons' (wrap xs'))
        Right x -> unwrap $ cons' x (wrap xs')
      )
      (unwrap xs)

lift :: List m => m a -> EitherT b m a
lift xs = EitherT $ List.map Right xs

liftEither :: List l => l (Either b a) -> EitherT b l a
liftEither = wrap

hideEither :: List l => EitherT b l (Either b a) -> EitherT b l a
hideEither xs = wrap $ List.map Monad.join $ unwrap xs

run :: EitherT b m a -> m (Either b a)
run = unwrap

throw :: List m => e -> EitherT e m b -> EitherT e m b
throw x xs = wrap $ cons (Left x) (unwrap xs)

catch :: List m => (e -> EitherT e m a -> EitherT e m a) -> EitherT e m a -> EitherT e m a
catch cons' xs = wrap $
  uncons
    nil
    (\e xs' -> case e of
      Left x -> unwrap (cons' x (wrap xs'))
      Right x -> cons (Right x) (unwrap (catch cons' (wrap xs')))
    )
    (unwrap xs)

down :: List m => EitherT b m a -> m a
down xs =
  uncons
    nil
    (\e xs' -> case e of
      Left _ -> down (wrap xs')
      Right x -> cons x (down (wrap xs')))
    (unwrap xs)

downEither :: List l => EitherT b l a -> l (Either b a)
downEither = unwrap

instance ToHaskellList m => ToHaskellList (EitherT b m) where
  toHaskellList = toHaskellList . down
