-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Control.List.Trans.Kleisli where

import Control.List(List(nil,cons,uncons))
import qualified Control.List as List
import Control.Arrow(Kleisli(..), runKleisli)

instance List m => List (Kleisli m a) where
  nil = Kleisli (\_ -> nil)
  cons x xs = Kleisli (\x' -> cons x $ runKleisli xs x')
  uncons nil' cons' xs = Kleisli $ \x ->
    uncons
      (runKleisli nil' x)
      (\x' xs' -> runKleisli (cons' x' (Kleisli (\_ -> xs'))) x)
      (runKleisli xs x)

tmp :: Kleisli [] Int Int
tmp = Kleisli (\x -> [1..x])

tmp2 :: Int -> [Int]
tmp2 x = runKleisli (List.map (*2) ((List.++) tmp (cons 314 tmp))) x
