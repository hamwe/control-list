-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Control.List.Trans.ContPlus where

import Control.List(
  List(nil, cons, uncons),
  map,
  (++),
  unit,
  bind,
  apply)
import Prelude hiding(map, (++))
import Control.Applicative(Alternative(empty, (<|>)))
import Control.Monad(MonadPlus(mzero, mplus))
import Data.Monoid(Monoid(mempty, mappend))

data Cont e l a = Cont ((l a -> l e -> l e) -> l e -> l e)

wrap = Cont
unwrap (Cont x) = x

instance List l => List (Cont e l) where
  nil = wrap $ \k -> k nil
  cons x xs = wrap $ \k -> unwrap xs (\xs' -> k (cons x xs'))
  uncons nilB consB xs = wrap $ \k -> unwrap xs $ \xs' f ->
    uncons
      (unwrap nilB k f)
      (\x xs'' -> unwrap (consB x $ lift xs'') k f)
      xs'

lift :: List l => l a -> Cont e l a
lift xs = wrap $ \k -> k xs

downFoldr :: List l => (l a -> l b -> l b) -> l b -> Cont b l a -> l b
downFoldr op e xs = unwrap xs op e

down :: List l => Cont a l a -> l a
down xs = downFoldr (++) nil xs

instance List l => Functor (Cont e l) where
  fmap = map

instance List l => Applicative (Cont e l) where
  pure = unit
  (<*>) = apply

instance List l => Monad (Cont e l) where
  return = unit
  (>>=) = bind

instance List l => Alternative (Cont e l) where
  empty = nil
  xs <|> ys = wrap $ \k f -> unwrap xs k (unwrap ys k f)

instance List l => MonadPlus (Cont e l) where
  mzero = nil
  mplus = (<|>)

instance List l => Semigroup (Cont e l a) where
  (<>) = (++)

instance List l => Monoid (Cont e l a) where
  mempty = nil
