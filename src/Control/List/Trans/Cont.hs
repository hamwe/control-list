-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Control.List.Trans.Cont(
  Cont,
  lift,
  down
) where

import Control.List(
  List(nil, cons, uncons),
  map,
  (++),
  unit,
  bind,
  apply)
import Prelude hiding(map, (++))
import Control.Applicative(Alternative(empty, (<|>)))
import Data.Monoid(Monoid(mempty))

data Cont e l a = Cont ((l a -> l e) -> l e)

wrap = Cont
unwrap (Cont x) = x

instance List l => List (Cont e l) where
  nil = wrap $ \k -> k nil
  cons x xs = wrap $ \k -> unwrap xs $ \xs' -> k (cons x xs')
  uncons nilB consB xs = wrap $ \k -> unwrap xs $ \xs' ->
    uncons
      (unwrap nilB k)
      (\x xs'' -> unwrap (consB x $ lift xs'') k)
      xs'

lift :: List l => l a -> Cont e l a
lift xs = wrap $ \k -> k xs

down :: Cont a l a -> l a
down m = unwrap m id

instance List l => Functor (Cont e l) where
  fmap = map

instance List l => Applicative (Cont e l) where
  pure = unit
  (<*>) = apply

instance List l => Monad (Cont e l) where
  return = unit
  (>>=) = bind

instance List l => Alternative (Cont e l) where
  empty = nil
  (<|>) = (++)

instance List l => Semigroup (Cont e l a) where
  (<>) = (++)

instance List l => Monoid (Cont e l a) where
  mempty = nil
