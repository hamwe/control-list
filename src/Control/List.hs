-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Control.List(
  List(nil, cons, uncons),
  ToList(toList),
  ToHaskellList(toHaskellList),
  (|>),
  (++),
  map,
  reverse,
  intersperse,
  foldr,
  foldrWithState,
  foldr',
  mapFoldr,
  foldl,
  foldl',
  mapFoldl,
  concat,
  concatMap,
  scanl,
  filter,
  head,
  last,
  tail,
  init,
  zip,
  zipWith,
  take,
  drop,
  lines,
  unlines,
  words,
  unwords,
  unit,
  bind,
  apply)
where

import Prelude hiding (
  (++),
  tail,
  map,
  reverse,
  intersperse,
  foldr,
  foldl,
  concat,
  concatMap,
  scanl,
  filter,
  head,
  last,
  tail,
  init,
  zip,
  zipWith,
  take,
  drop,
  lines,
  unlines,
  words,
  unwords)

import Data.Char(isSpace)

infixl 1 |>
(|>) :: a -> (a -> b) -> b
x |> f = f x

-- uncons nil' cons' nil = nil'
-- uncons nil' cons' (cons x xs) = cons' x xs
-- uncons nil' cons' xs = nil' | exist x xs' (uncons nil' cons' xs = cons x xs')
class List m where
  nil :: m a
  cons :: a -> m a -> m a
  uncons :: m b -> (a -> m a -> m b) -> m a -> m b

-- The two below (lunit, lbind) just to show similarities with the monad operations
lunit :: List l => Maybe (a, l a) -> l a
lunit Nothing = nil
lunit (Just (x,xs)) = cons x xs

-- In a way the lbind (and uncons) does to a MonadPlus what the bind does to a Monad
lbind :: List l => l a -> (Maybe (a, l a) -> l b) -> l b
lbind xs f = uncons (f Nothing) (\x xs' -> f (Just (x,xs'))) xs

instance List [] where
  nil = []
  cons = (:)
  uncons x _ [] = x
  uncons _ f (x:xs) = f x xs

class ToList m where
  toList :: List lst => m a -> lst a

instance ToList [] where
  toList [] = nil
  toList (x:xs) = cons x (toList xs)

instance ToList Maybe where
  toList Nothing = nil
  toList (Just x) = cons x nil

instance ToList (Either b) where
  toList (Left _) = nil
  toList (Right x) = cons x nil

class List m => ToHaskellList m where
  toHaskellList :: m a -> [a]

instance ToHaskellList [] where
  toHaskellList = id

unconsWithState :: List l => (s -> l b) -> (a -> l a -> s -> l b) -> l a -> s -> l b
unconsWithState nil' cons' cs s = uncons
  (nil' s)
  (\c cs' -> cons' c cs' s)
  cs

(++) :: List m => m a -> m a -> m a
xs ++ ys = uncons ys (\x xs' -> cons x (xs' ++ ys)) xs

map :: List m => (a -> b) -> m a -> m b
map f = uncons nil (\x xs -> cons (f x) (map f xs))

reverse :: List m => m a -> m a
reverse = aux nil
  where aux xs = uncons xs (\x -> aux (cons x xs))

intersperse :: List m => a -> m a -> m a
intersperse e = uncons nil (\x xs -> cons x (aux xs))
  where aux = uncons nil (\x xs -> cons e $ cons x $ aux xs)

foldr :: List m => (a -> m b -> m b) -> m b -> m a -> m b
foldr op e = uncons e (\x xs -> op x (foldr op e xs))

foldrWithState :: List l => (a -> (s -> l b) -> s -> l b) -> (s -> l b) -> l a -> s -> l b
foldrWithState op e = unconsWithState nil' cons'
  where nil' s = e s
        cons' c cs s = op c (foldrWithState op e cs) s

foldr' :: List l => (a -> b -> b) -> b -> l a -> l b
foldr' op e =
  uncons
    (cons e nil)
    (\x xs -> uncons undefined (\x' _ -> cons (op x x') nil) (foldr' op e xs))

mapFoldr :: List l => (a -> b -> b) -> b -> l (l a) -> l b
mapFoldr op e = concatMap (foldr' op e)

foldl :: List m => (m b -> a -> m b) -> m b -> m a -> m b
foldl op e = uncons e (\x xs -> foldl op (op e x) xs)

foldl' :: List l => (b -> l c) -> (b -> a -> b) -> b -> l a -> l c
foldl' f op e = uncons (f e) (\x xs -> foldl' f op (op e x) xs)

mapFoldl :: List l => (b -> a -> b) -> b -> l (l a) -> l b
mapFoldl op e = uncons nil (\xs xss -> foldl' (\y -> cons y (mapFoldl op e xss)) op e xs)

concat :: List m => m (m a) -> m a
concat = uncons nil (\xs xss -> xs ++ concat xss)

concatMap :: List m => (a -> m b) -> m a -> m b
concatMap f xs = concat (map f xs)

scanl :: List m => (b -> a -> b) -> b ->  m a -> m b
scanl op e = cons e . uncons nil (\x -> scanl op (op e x))

filter :: List m => (a -> Bool) -> m a -> m a
filter p = uncons nil (\x xs -> if p x then cons x (filter p xs) else filter p xs)

head :: List l => l a -> l a
head = uncons undefined (\x _ -> cons x nil)

last :: List l => l a -> l a
last = uncons undefined last_
  where last_ x = uncons (cons x nil) (\x' xs -> last_ x' xs)

tail :: List l => l a -> l a
tail = uncons undefined (\x xs -> xs)

init :: List l => l a -> l a
init = uncons undefined init_
  where init_ x = uncons nil (\x' xs -> cons x (init_ x' xs))

zip :: List m => m a -> m b -> m (a,b)
zip xs ys = uncons nil (\x xs' -> uncons nil (\y ys' -> cons (x,y) (zip xs' ys')) ys) xs

zipWith :: List m => (a -> b -> c) -> m a -> m b -> m c
zipWith f xs ys = map (uncurry f) (zip xs ys)

take :: List m => Int -> m a -> m a
take n | n > 0 = uncons nil (\x -> cons x . take (n-1))
take _ = const nil

drop :: List m => Int -> m a -> m a
drop n | n > 0 = uncons nil (\_ -> drop (n-1))
drop _ = id

words :: List l => l Char -> l String
words cs = foldrWithState op e cs ""
  where e "" = nil
        e cs = cons (reverse cs) nil
        op c f cs | isSpace c && cs /= "" = cons (reverse cs) (f "")
                  | isSpace c && cs == "" = f ""
                  | otherwise = f (c:cs)

unwords :: List l => l String -> l Char
unwords = concatMap toList . intersperse " "

lines :: List l => l Char -> l String
lines cs = foldrWithState op e cs ""
  where e "" = nil
        e cs = cons (reverse cs) nil
        op '\n' f cs = cons (reverse cs) (f "")
        op c f cs = f (c:cs)

unlines :: List l => l String -> l Char
unlines = foldr (\cs cs' -> toList cs ++ cons '\n' cs') nil

unit :: List l => a -> l a
unit x = cons x nil

bind :: List l => l a -> (a -> l b) -> l b
bind = flip concatMap

apply :: List l => l (a -> b) -> l a -> l b
apply fs xs = concatMap (\f -> map (\x -> f x) xs) fs
