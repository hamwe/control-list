-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Control.Monad.Trans.Fold.Class where

import Control.Monad.Trans.Class(MonadTrans)

class MonadTrans t => TransFold t where
  fold :: Monad m => (b -> a -> m b) -> b -> t m a -> m b
  unfold :: Monad m => (b -> m (Maybe (a, b))) -> b -> t m a

-- fold op e empty = pure e
-- fold op e (pure x) = op e x
-- fold op e (xs <|> ys) = do e' <- fold op e xs; fold op e' ys

foldl :: (TransFold t, Monad m) => (b -> a -> b) -> b -> t m a -> m b
foldl op e = fold (\x y -> let z = op x y in z `seq` pure z) e

foldUnfold :: Monad m => (b -> a -> m b) -> (s -> m (Maybe (a, s))) -> s -> b -> m b
foldUnfold op f = go where
  go s e = f s >>= maybe (return e) (\(x, s') -> op e x >>= \e' -> e' `seq` go s' e')

-- fold op e (unfold f s) = foldUnfold op f s e

foreach :: (Monad m, TransFold t) => t m a -> (a -> m ()) -> m ()
foreach xs f = fold (\() x -> f x) () xs

repeat :: (Monad m, TransFold t) => m a -> t m a
repeat m = unfold (\() -> do x <- m; return (Just (x, ()))) ()

maybeRepeat :: (Monad m, TransFold t) => m (Maybe a) -> t m a
maybeRepeat m = unfold (\() -> m >>= maybe (return Nothing) (\x -> return (Just (x, ())))) ()
