{-# LANGUAGE RankNTypes #-}

module Control.Monad.Trans.Fold where

import Control.Applicative(Alternative(empty, (<|>)))
import Control.Monad(MonadPlus)
import Control.Monad.Trans.Class(MonadTrans(lift))
import Control.Monad.Trans.Fold.Class(TransFold(fold, unfold))
import Control.Monad.IO.Class(MonadIO(liftIO))

type FoldE m a = forall e . (a -> e -> m e) -> e -> m e

data Fold m a = Fold (FoldE m a)

wrap :: FoldE m a -> Fold m a
wrap = Fold

unwrap :: Fold m a -> FoldE m a
unwrap (Fold x) = x

instance Functor (Fold m) where
  fmap f m = wrap $ \k -> unwrap m $ \x -> k (f x)

instance Applicative (Fold m) where
  pure x = wrap $ \k -> k x
  mf <*> mx = wrap $ \k -> unwrap mf $ \f -> unwrap mx $ \x -> k (f x)

instance Monad (Fold m) where
  return = pure
  m >>= f = wrap $ \k -> unwrap m $ \x -> unwrap (f x) k

instance Monad m => Semigroup (Fold m a) where
  m <> n = wrap $ \k e -> unwrap m k e >>= unwrap n k

instance Monad m => Monoid (Fold m a) where
  mempty = wrap $ \k -> pure

instance Monad m => Alternative (Fold m) where
  empty = mempty
  (<|>) = (<>)

instance Monad m => MonadPlus (Fold m)

instance MonadTrans Fold where
  lift m = wrap $ \k e -> m >>= \x -> k x e

instance TransFold Fold where
  fold op e m = unwrap m (flip op) e
  unfold f b = wrap $ \k e ->
    let go b e = f b >>= maybe (return e) (\(x, b') -> k x e >>= go b')
    in go b e

instance MonadIO m => MonadIO (Fold m) where
  liftIO = lift . liftIO

convert :: (TransFold t, Monad m) => t m a -> Fold m a
convert xs = Fold (\op e -> fold (flip op) e xs)

-- fold op e (convert xs) = fold op e xs
