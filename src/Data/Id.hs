-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Data.Id(Id, eval) where

data Id a = Id a deriving(Eq, Read, Show)

wrap = Id
unwrap (Id x) = x

instance Functor Id where
  fmap f m = wrap $ f $ unwrap m

instance Applicative Id where
  pure = wrap
  fm <*> xm = wrap $ unwrap fm $ unwrap xm

instance Monad Id where
  return = wrap
  m >>= f = f $ unwrap m

eval :: Id a -> a
eval = unwrap
