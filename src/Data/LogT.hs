-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Data.LogT(
  LogT
, lift
, eval
, evalLog
, logChar
, logStr
, logStrLn
) where

data LogT m a = LogT (String -> m (a, String))

wrap = LogT
unwrap (LogT m) = m

instance Functor m => Functor (LogT m) where
  fmap f m = wrap $ \s -> fmap (\(x,s') -> (f x, s')) $ unwrap m s

instance Monad m => Applicative (LogT m) where
  pure x = wrap $ \s -> pure (x, s)
  fm <*> fx = wrap $ \s -> do
    (f, s') <- unwrap fm s
    (x, s'') <- unwrap fx s'
    return (f x, s'')

instance Monad m => Monad (LogT m) where
  return = pure
  m >>= f = wrap $ \s -> do
    (x, s') <- unwrap m s
    unwrap (f x) s'

logChar :: Monad m => Char -> LogT m ()
logChar c = wrap $ \s -> return ((), (c:s))

logStr :: Monad m => String -> LogT m ()
logStr cs = wrap $ \s -> return ((), reverse cs ++ s)

logStrLn :: Monad m => String -> LogT m ()
logStrLn cs = logStr (cs ++ "\n")

lift :: Monad m => m a -> LogT m a
lift m = wrap $ \s -> do x <- m; return (x, s)

eval :: Monad m => LogT m a -> m a
eval m = do (x,_) <- unwrap m ""; return x

evalLog :: Monad m => LogT m a -> m (a, String)
evalLog m = do (x, s) <- unwrap m ""; return (x, reverse s)
