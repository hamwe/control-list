-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Data.WrapList where

import Control.List(
  List(nil, cons, uncons),
  map,
  (++),
  unit,
  bind,
  apply)
import Prelude hiding(map, (++))
import Control.Applicative(Alternative(empty, (<|>)))
import Data.Monoid(Monoid(mempty, mappend))
import Control.Monad(MonadPlus(mzero, mplus))

data Wrap m a = Wrap (m a)

wrap = Wrap
unwrap (Wrap x) = x

instance List l => List (Wrap l) where
  nil = wrap nil
  cons x xs = wrap $ cons x $ unwrap xs
  uncons nilB consB xs = wrap $
    uncons
      (unwrap nilB)
      (\x xs' -> unwrap $ consB x $ wrap xs')
      (unwrap xs)

instance List l => Functor (Wrap l) where
  fmap = map

instance List l => Applicative (Wrap l) where
  pure = unit
  (<*>) = apply

instance List l => Monad (Wrap l) where
  return = unit
  (>>=) = bind

instance List l => Alternative (Wrap l) where
  empty = nil
  (<|>) = (++)

instance List l => MonadPlus (Wrap l) where
  mzero = nil
  mplus = (++)

instance List l => Semigroup (Wrap l a) where
  (<>) = (++)

instance List l => Monoid (Wrap l a) where
  mempty = nil
