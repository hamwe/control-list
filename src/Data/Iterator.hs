-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Data.Iterator(
  Iterator,
  iterator,
  lift,
  liftMaybe,
  downUncons)
where

import Control.Monad.Trans.Class(MonadTrans(lift))
import Control.List(List(nil, cons, uncons))
import Control.Monad.Trans.Fold.Class(TransFold(fold, unfold))

data Iterator m a = Iterator (m (Maybe (a, Iterator m a)))

wrap = Iterator
unwrap (Iterator m) = m

instance Monad m => List (Iterator m) where
  nil = wrap $ pure Nothing
  cons x xs = wrap $ pure $ Just (x, xs)
  uncons nilB consB xs = wrap (unwrap xs >>= f)
    where f Nothing = unwrap nilB
          f (Just (x,xs')) = unwrap (consB x xs')

iterator :: Monad m => m (Maybe (a, Iterator m a)) -> Iterator m a
iterator = Iterator

instance MonadTrans Iterator where
  lift m = wrap $ do x <- m; return (Just (x, wrap $ return Nothing))

liftMaybe :: Monad m => m (Maybe a) -> Iterator m a
liftMaybe m = wrap $ m >>= (pure . fmap (\x -> (x, liftMaybe m)))

downUncons :: Monad m => m b -> (a -> Iterator m a -> m b) -> Iterator m a -> m b
downUncons nilM consM xs = do
  mb <- (unwrap xs)
  case mb of
    Nothing -> nilM
    Just (x,xs) -> consM x xs

instance TransFold Iterator where
  fold op e = downUncons (return e) (\x xs -> do e' <- op e x; fold op e' xs)
  unfold f = go where
    go s = iterator (f s >>= maybe (return Nothing) (\(x,s') -> return (Just (x, go s'))))
