module Example.Sum10FirstIntegers(
  example,
  readInt
) where

import Prelude hiding(
  words,
  unlines,
  map,
  scanl,
  take,
  )

import Control.List(
  List(nil, cons, uncons),
  (|>),
  map,
  scanl,
  take,
  words,
  unlines
  )

import Control.List.Trans.EitherT(EitherT, liftEither, downEither)

readInt :: String -> Either String Int
readInt cs = case reads cs of
  [(i,"")] -> Right i
  _        -> Left (cs ++ " is not an integer")

example :: List l => l Char -> l Char
example cs =
     words cs
  |> map readInt
  |> liftEither
  |> scanl (+) 0
  |> take 10
  |> downEither
  |> map (either id show)
  |> unlines
