-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Example.IteratorExample(
  interact,
  lineInteract,
  fromStdin,
  toStdout,
  linesFromStdin,
  exampleStdinToStdout,
  fromFileHandle,
  toFileHandle,
  exampleFromFileHandleToFileHandle)
where

import Prelude hiding(take, concatMap, (++), interact)
import Control.List((|>), take, concatMap, toList, (++))
import Control.Monad.Trans.Fold.Class(TransFold, maybeRepeat, foreach)
import Data.Iterator(Iterator, liftMaybe)
import System.IO(
  isEOF,
  getChar,
  putChar,
  getLine,
  Handle,
  hIsEOF,
  hGetChar,
  openFile,
  IOMode(ReadMode, WriteMode),
  hClose,
  hPutChar)
import System.Directory(removeFile)
import System.Random(randomIO)

toStdout :: TransFold t => t IO Char -> IO ()
toStdout = flip foreach putChar

ifM :: Monad m => m Bool -> m a -> m a -> m a
ifM m x y = do c <- m; if c then x else y

stdinNext :: IO (Maybe Char)
stdinNext = ifM isEOF (return Nothing) (fmap Just getChar)

fromStdin :: TransFold t => t IO Char
fromStdin = maybeRepeat stdinNext

linesFromStdin :: Iterator IO String
linesFromStdin = liftMaybe (ifM isEOF (return Nothing) (fmap Just getLine))

interact :: TransFold t => (t IO Char -> t IO Char) -> IO ()
interact f = fromStdin |> f |> toStdout

lineInteract :: (Iterator IO Char -> Iterator IO Char) -> IO ()
lineInteract consumer =
  linesFromStdin
  |> concatMap (\line -> toList (line ++ "\n"))
  |> consumer
  |> toStdout

-- Reads from stdin line by line and prints the first 10 characters to stdout.
exampleStdinToStdout :: IO ()
exampleStdinToStdout =
  linesFromStdin
  |> concatMap (\line -> toList (line ++ "\n"))
  |> take 10
  |> toStdout

fromFileHandle :: Handle -> Iterator IO Char
fromFileHandle handle = liftMaybe $
  ifM (hIsEOF handle) (return Nothing) (fmap Just (hGetChar handle))

toFileHandle :: Handle -> Iterator IO Char -> IO ()
toFileHandle handle = flip foreach (hPutChar handle)

exampleFromFileHandleToFileHandle :: IO ()
exampleFromFileHandleToFileHandle = do
  fileName <- randomFileName
  outHandle <- openFile fileName WriteMode
  content |> toFileHandle outHandle
  hClose outHandle
  inHandle <- openFile fileName ReadMode
  fromFileHandle inHandle |> toStdout
  hClose inHandle
  removeFile fileName
  where content = toList "Some kind of content\n"

randomFileName :: IO String
randomFileName = do i <- randomIO :: IO Int; return ("/tmp/" ++ show (abs i))

exampleFromFileHandle = do
  handle <- openFile "Setup.hs" ReadMode
  fromFileHandle handle |> toStdout
  hClose handle
