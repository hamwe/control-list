module Example.WordCount(count, wordCount) where

import Control.List(List(nil, cons), (|>), words, map, unlines, foldrWithState)
import Prelude hiding(words, map, unlines)
import Data.Map.Strict(Map)
import qualified Data.Map.Strict as Map(empty, lookup, insert)

wordCount :: List l => l Char -> l Char
wordCount input =
  words input
  |> count
  |> map show
  |> unlines

count :: (List l, Ord a) => l a -> l (Int, a)
count xs = foldrWithState op e xs Map.empty
  where e _ = nil
        op x f oldMap = cons (newCount, x) (f newMap)
          where (newCount, newMap) = countElement x oldMap

countElement :: Ord a => a -> Map a Int -> (Int, Map a Int)
countElement x oldMap = (newCount, newMap)
  where newCount = maybe 0 id (Map.lookup x oldMap) + 1
        newMap = Map.insert x newCount oldMap
