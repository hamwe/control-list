-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module ListSpec where

import Test.Hspec(
    Spec
  , it
  , shouldBe
  )
import Test.QuickCheck(property)

import Control.List(
  List(nil, cons, uncons), head, last, tail, init, take, drop, lines, unlines,
  words, unwords, foldl, foldr, foldr', scanl, foldl', mapFoldr, mapFoldl,
  intersperse)
import Prelude hiding(
  head, last, tail, init, take, drop, lines, unlines, words, unwords, foldl,
  foldr, scanl)
import qualified Prelude as Prelude(
  head, last, tail, init, take, drop, lines, unlines, words, unwords, foldl,
  foldr, scanl)
import qualified Data.List as List(intersperse)

linesShouldBe input = lines input `shouldBe` Prelude.lines input
wordsShouldBe input = words input `shouldBe` Prelude.words input

propFoldl' f op e xs = foldl' f op e xs == f (Prelude.foldl op e xs)

propMapFoldl op e xss = mapFoldl op e xss == map (Prelude.foldl op e) xss
propMapFoldr op e xss = mapFoldr op e xss == map (Prelude.foldr op e) xss

spec :: Spec
spec = do
  it "foldr" $ property $ \xs-> foldr cons nil xs == Prelude.foldr (:) [] (xs :: [Int])
  it "foldr'" $ property $ \xs-> foldr' (-) 0 xs == [Prelude.foldr (-) 0 (xs :: [Int])]
  it "mapFoldr" $ property $ \xss -> propMapFoldr (-) 0 (xss :: [[Int]])
  it "foldl" $ property $ \xs-> foldl (flip cons) nil xs == Prelude.foldl (flip (:)) [] (xs :: [Int])
  it "foldl'" $ property $ \xs -> propFoldl' pure (-) 0 (xs :: [Int])
  it "mapFoldl" $ property $ \xss -> propMapFoldl (-) 0 (xss :: [[Int]])
  it "scanl" $ property $ \xs -> scanl (-) 0 xs == Prelude.scanl (-) 0 (xs :: [Int])
  it "head" $ property $ \x xs -> head (x:xs) == [Prelude.head (x:xs :: [Int])]
  it "last" $ property $ \x xs -> last (x:xs) == [Prelude.last (x:xs :: [Int])]
  it "tail" $ property $ \x xs -> tail (x:xs) == Prelude.tail (x:xs :: [Int])
  it "init" $ property $ \x xs -> init (x:xs) == Prelude.init (x:xs :: [Int])
  it "take" $ property $ \n xs -> take n xs == Prelude.take n (xs :: [Int])
  it "drop" $ property $ \n xs -> drop n xs == Prelude.drop n (xs :: [Int])
  it "lines" $ property $ \xs -> lines xs == Prelude.lines xs
  it "unlines" $ property $ \xs -> unlines xs == Prelude.unlines xs
  it "words" $ property $ \xs -> words xs == Prelude.words xs
  it "unwords" $ property $ \xs -> unwords xs == Prelude.unwords xs
  it "intersperse" $ property $ \x xs -> intersperse x xs == List.intersperse x (xs :: [Int])
