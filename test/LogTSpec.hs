-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module LogTSpec(spec) where

import Data.LogT(
    LogT
  , evalLog
  , logStr
  )

import Test.Hspec(
    Spec
  , describe
  , it
  , shouldBe
  )

import Data.Id(Id, eval)

toLog :: LogT Id () -> String
toLog m = snd $ eval $ evalLog m

spec :: Spec
spec = do
  describe "LogT" $ do
    it "log" $ do
      toLog (logStr "bla" >> logStr "ha") `shouldBe` "blaha"
