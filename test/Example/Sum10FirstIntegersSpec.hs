module Example.Sum10FirstIntegersSpec where

import Test.Hspec(
    Spec
  , describe
  , it
  , shouldBe
  )

import Example.Sum10FirstIntegers(example, readInt)

spec :: Spec
spec = do
  describe "Example.Sum10FirstIntegers" $ do
    it "example" $ do
      example "1 2 sd 3" `shouldBe` "0\n1\n3\nsd is not an integer\n6\n"
    it "readInt" $ do
      readInt "22" `shouldBe` (Right 22)
      readInt "22s" `shouldBe` (Left "22s is not an integer")
