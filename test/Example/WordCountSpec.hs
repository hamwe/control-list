module Example.WordCountSpec(spec) where

import Test.Hspec(
    Spec
  , describe
  , it
  , shouldBe
  )

import Example.WordCount(count, wordCount)

spec :: Spec
spec = do
  describe "Example.WordCount" $ do
    it "wordCount" $ do
      wordCount "" `shouldBe` ""
      wordCount "a" `shouldBe` "(1,\"a\")\n"
      wordCount "a b a" `shouldBe` "(1,\"a\")\n(1,\"b\")\n(2,\"a\")\n"
    it "count" $ do
      count ([] :: [String]) `shouldBe` []
      count ["a"] `shouldBe` [(1,"a")]
      count ["a","b","a"] `shouldBe` [(1,"a"),(1,"b"),(2,"a")]
      count ["a","b","a","c","b"] `shouldBe` [(1,"a"),(1,"b"),(2,"a"),(1,"c"),(2,"b")]
