-- Copyright 2018, Hampus Weddig <hampus.weddig@gmail.com>.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--        http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.

module Main where

import Example.Sum10FirstIntegers(example)
import Data.Iterator(Iterator)
import qualified Example.IteratorExample as Iterator(interact)

main :: IO ()
main = do putStrLn ("Showing example with just the string " ++ show string)
          putStrLn $ example string
          putStrLn "Now enter some positive integers"
          Iterator.interact (example :: (Iterator IO Char -> Iterator IO Char))
  where string = "1\ninput with 2\n3 4 some 5 6 invalid \n7 8 9 integers 10 11 12"
